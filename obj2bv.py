"""
OBJ TO BV FILE CONVERTER

Input: .obj file
Output: .bv file using polygon format (type=1)

Please find more detail about BView in UF SurfLab website
https://www.cise.ufl.edu/research/SurfLab/bview/
"""

import re
import sys

objFileName = sys.argv[1].rstrip('.obj')
bvFileName = sys.argv[2] if len(sys.argv) > 2 else objFileName + '_net.bv'

bvFile = open(bvFileName, 'w')
objFile = open(objFileName + '.obj', 'r')

numOfVert = 0
numOfFace = 0

vertData = ''
faceData = ''

tline = objFile.readline()
while tline:
    if tline.startswith('v '):
        tline = tline.replace('v ', '')
        vertData += tline
        numOfVert += 1
    elif tline.startswith('f '):
        tline = re.sub(r'/(/)?\d+', '', tline)
        tline = re.sub(r'\d+', lambda x: str(int(x.group(0))-1), tline)
        numOfVertPerFace = len(re.findall(r'\d+', tline))
        tline = tline.replace('f', str(numOfVertPerFace))
        faceData += tline
        numOfFace += 1
    tline = objFile.readline()

bvFile.write('Group 10 Control_net \n')
bvFile.write('1 \n') # display type polygon = 1
bvFile.write(str(numOfVert) + " " + str(numOfFace) + '\n')
bvFile.write(vertData)
bvFile.write(faceData)

bvFile.close()
objFile.close()
