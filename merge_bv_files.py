import sys

def merge_bv_files(input_patch_file, input_control_net_file, output_file=None):
    if output_file is None:
        output_file = input_patch_file[:-3] + "_patch+control_net.bv"
    assert input_patch_file.endswith(".bv") and input_control_net_file.endswith(".bv"), "Input files must have .bv extension"

    with open(input_patch_file, "r") as patch_file, open(input_control_net_file, "r") as control_net_file:
        patch_data = patch_file.read()
        control_net_data = control_net_file.read()

    with open(output_file, "w") as outfile:
        outfile.write(patch_data + control_net_data)

if __name__ == "__main__":
    input_patch_file = sys.argv[1]
    input_control_net_file = sys.argv[2]
    try:
        output_file = sys.argv[3]
    except IndexError:
        output_file = None
    merge_bv_files(input_patch_file, input_control_net_file, output_file)
