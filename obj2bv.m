%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OBJ TO BV FILE CONVERTER                                                %
%                                                                         %
% Input: .obj file                                                        %
% Output: .bv file using polygon format (type=1)                          %
%                                                                         %
% Please find more detail about BView in UF SurfLab website               %
% https://www.cise.ufl.edu/research/SurfLab/bview/                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

objFileName = 'test';

bvFile = fopen(strcat(objFileName, '_net','.bv'),'w');
objFile = fopen(strcat(objFileName,'.obj'),'r');

numOfVert = 0;
numOfFace = 0;

vertData = '';
faceData = '';

tline = fgetl(objFile);
while ischar(tline)
    if startsWith(tline,'v ')
        tline = regexprep(tline,'v ', '');
        vertData = strcat(vertData, tline, '\n');
        numOfVert = numOfVert + 1;
    elseif startsWith(tline,'f ')
        
        % Remove vertex normal indices 
        tline = regexprep(tline,'/(/)?\d+','');
        
        % Minus 1 for each vertex's id (.obj starts from 1 but .bv from 0)
        tline = regexprep(tline,'\d+','${num2str(str2num($0)-1)}');
        
        % replace f symbol in .obj as num_of_vert per face in .bv
        numOfVertPerFace = numel(regexp(tline,'\d+','match'));
        tline = regexprep(tline,'f', num2str(numOfVertPerFace));
        
        faceData = strcat(faceData, tline, '\n');
        numOfFace = numOfFace + 1;
    end
    tline = fgetl(objFile);
end

fprintf(bvFile, 'Group 10 Control_net \n');
fprintf(bvFile, '1 \n'); % display type polygon = 1
fprintf(bvFile, strcat(num2str(numOfVert), " ", num2str(numOfFace), '\n'));
fprintf(bvFile, vertData);
fprintf(bvFile, faceData);