#!/bin/bash

# Define the folder to search in
folder_path="./"

for file in "$folder_path"/*.bv
do
  filename_ext="${file##*/}"
  filename="${filename_ext%.*}"

  python obj2bv.py "${folder_path}/${filename}_control_net.obj" "${folder_path}/${filename}_control_net.bv"

  python merge_bv_files.py "${folder_path}/${filename_ext}" "${folder_path}/${filename}_control_net.bv"

  # echo "Filename without extension: $filename"
done
