# OBJ2BV #

This program helps coverting .obj file into .bv file. 
That enable users to view polygon/mesh in [BView](https://www.cise.ufl.edu/research/SurfLab/bview/)

## Auto merge each control net .obj and patch .bv pair into single .bv file ##
1. Set `folder_path` in `generate_patch_control_net_bv.sh` to the directory store all pairs of `[filename]_control_net.obj` and `[filename]_patch.bv`
2. Run `generate_patch_control_net_bv.sh` to process all control net and patch pair. The output will be `[filename]_patch+control_net.bv`

## Convert obj to bv (Python) ##
```
python obj2bv.py input.obj output.bv
```

## Convert obj to bv (Matlab) ##

1. Download obj2bv.m and put it into the same folder with .obj file.
2. Open obj2bv.m with Matlab
3. Set objFileName as the name of .obj file (extension not included)
4. Execute the program

* Input: \[filename\].obj
* Output: \[filename\]\_net.bv

## How to display control polygon (net) and Bernstein B�zier (BB-)patch at the same time? ##

There are two ways:
1. Use `python merge_bv_files.py ipnut1.bv ipnut2.bv output.bv`
2. Copy all the content of \[filename\]\_net.bv and append to \[filename\]\_patch.bv (using any text editor e.g. Atom)

Example:

**A_net.bv** (control polygon)
```
Group 10 Control_net 
1 
9 4
-0.5 -0.0 0.5
0.0 -0.0 0.5
0.5 -0.0 0.5
-0.5 0.0 0.0
0.0 0.0 0.0
0.5 0.0 0.0
-0.5 0.0 -0.5
0.0 0.0 -0.5
0.5 0.0 -0.5
4 0 1 4 3
4 1 2 5 4
4 3 4 7 6
4 4 5 8 7
```

**A_patch.bv** (BB-patch)
```
Group 0 Regular
5
2 2
0.25 0.00 -0.25
0.25 0.00 0.00
0.25 0.00 0.25
0.00 0.00 -0.25
0.00 0.00 0.00
0.00 0.00 0.25
-0.25 0.00 -0.25
-0.25 0.00 0.00
-0.25 0.00 0.25
```

We can cascade **A_net.bv** and **A_patch.bv** as 

**A.bv**

```
Group 10 Control_net 
1 
9 4
-0.5 -0.0 0.5
0.0 -0.0 0.5
0.5 -0.0 0.5
-0.5 0.0 0.0
0.0 0.0 0.0
0.5 0.0 0.0
-0.5 0.0 -0.5
0.0 0.0 -0.5
0.5 0.0 -0.5
4 0 1 4 3
4 1 2 5 4
4 3 4 7 6
4 4 5 8 7
Group 0 Regular
5
2 2
0.25 0.00 -0.25
0.25 0.00 0.00
0.25 0.00 0.25
0.00 0.00 -0.25
0.00 0.00 0.00
0.00 0.00 0.25
-0.25 0.00 -0.25
-0.25 0.00 0.00
-0.25 0.00 0.25
```

Display A.bv in [BView](https://www.cise.ufl.edu/research/SurfLab/bview/)

![Alt text](ply_bb.png)

All right reserved by Surflab@UF